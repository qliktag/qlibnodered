/**
 * Copyright JS Foundation and other contributors, http://js.foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
module.exports = function(RED) {
    "use strict";
    var bodyParser = require("body-parser");
    var multer = require("multer");
    var cookieParser = require("cookie-parser");
    var getBody = require('raw-body');
    var cors = require('cors');
    var onHeaders = require('on-headers');
    var typer = require('media-typer');
    var isUtf8 = require('is-utf8');
    var hashSum = require("hash-sum");
    var uuid = require("uuid");
    var elasticsearch = require("@elastic/elasticsearch");
    var async = require("async");

    var mongoose = require('mongoose');
    const oauthSchemaObject = mongoose.Schema({
        controllerId: String,
        accessToken: String,
        refreshToken: String,
        userId: String,
        accountId: String,
        accountUserId: String,
        user: {
            "userId": String
        },
        applicationId: String,
        applicationuserId: String,
        expiresAt: { type: Date },
        isController: Boolean,
        controllerAccountId: String,
        controllerAccountUserId: String,
        createdBy: { type: Object },
        createdDate: { type: Date },
        __v: { type: Number, select: false }
    }, {
        collection: 'oauth'
    });

    let oauthSchema = mongoose.model('oauthCustom', oauthSchemaObject); //oauthCustom is same oauth collection just the name which is different

    let saveTrackingData = (msg, statusCode) => new Promise((resolve, reject) => {
        let oauthObj = {};
        let statisticsResponse = {};
        let esId = "";
        let clientCommon = {};
        let dbHost = msg.req.tempStore.ES_COMMON_URL;
        let indexName = "tracking";
        async.series([
            callback => {
                //if token there extract the details and store in tempstore 
                if (msg.req.headers.authorization) {
                    let bearerToken = msg.req.headers.authorization;
                    let split = bearerToken.split(" ");
                    let authorization = split[1];
                    let query = {};
                    query.controllerId = msg.req.tempStore.controllerId;
                    query.accessToken = authorization;
                    oauthSchema.findOne(query, (err, response) => {
                        if (err) {
                            console.error(`[>>>] Error occurred at fetching oauth token, raising errorMessage: ${err}`);
                            callback(err);
                        } else {
                            console.log(`[+++] Authorization token fetched successfully`);
                            msg.req.tempStore.oauthObject = response;
                            callback();
                        }
                    });
                } else {
                    callback();
                }
            },
            callback => {
                // add tracking in ES after data preparation
                clientCommon = new elasticsearch.Client({
                    node: dbHost
                });
                let statisticsData = collectTrackingInfo(msg, statusCode);
                clientCommon.index({
                    index: indexName,
                    type: `_doc`,
                    body: statisticsData,
                    id: uuid.v1()
                }).then(statisticsSavedResult => {
                    let result = transFormElasticSearchResponse(statisticsSavedResult, "success");
                    if (result.status == false) {
                        callback({ errorCode: "GEN007" });
                    } else {
                        statisticsSavedResult = result.data;
                        esId = statisticsSavedResult._id;
                        statisticsResponse = { "statisticsId": statisticsSavedResult._id, "status": statisticsSavedResult.status };
                        callback();
                    }
                }, statisticsSavedError => {
                    console.log(`[>>>] Error at statisticsSavedResult ${JSON.stringify(statisticsSavedError)}`);
                    callback({ errorCode: "GEN007" });
                });
            }
        ], (err, done) => {
            if (err) {
                console.error(`[>>>] Error occurred at Adding tracking information for custom API, raising errorMessage: ${err}`);
                resolve();
            } else {
                console.log(`[+++] Successfully added tracking information with tracking statisticsId ${statisticsResponse.statisticsId}`)
                resolve();
            }
        });
    });

    function transFormElasticSearchResponse(response, responseType) {
        try {
            if (responseType == "success") {
                return { "status": true, "data": response.body };
            } else {
                return { "status": false, "data": response.meta.body.error };
            }
        } catch (e) {
            console.error(`[>>>] Error occured while transforming elastic search response, errorMessage: ${JSON.stringify(e)}`);
            return { "status": false, "data": "Error occured while transforming elastic search response" };
        }
    }

    function collectTrackingInfo(msg, statusCode) {

        var requestData = msg.req;
        requestData.tempStore.componentType = "customflows";

        var responseData = { respone: msg.payload };
        let responseSize = getSizeOfResponse(responseData);

        let ipAddr = '';
        let accountDetails = {};
        let componentDetails = {};
        let clientDetails = {};
        let requestDetails = {};
        let geoDetails = {};
        let trackingInfoObj = {};
        let lat = 0;
        let lon = 0;
        let geo = {};

        //Account Details 
        if (requestData.tempStore && requestData.tempStore.controllerInfo) {
            if (requestData.tempStore.controllerInfo._id) {
                accountDetails.controllerId = requestData.tempStore.controllerInfo._id;
            }

            if (requestData.tempStore.controllerInfo.controllerName) {
                accountDetails.controllerName = requestData.tempStore.controllerInfo.controllerName;
            }
        }

        if (requestData.tempStore && requestData.tempStore.oauthObject) {
            if (requestData.tempStore.oauthObject.accountId) {
                accountDetails.accountId = requestData.tempStore.oauthObject.accountId;
            }

            if (requestData.tempStore.oauthObject.userId) {
                accountDetails.userId = requestData.tempStore.oauthObject.userId;
            }

            if (requestData.tempStore.oauthObject.accountUserId) {
                accountDetails.accountUserId = requestData.tempStore.oauthObject.accountUserId;
            }

            if (requestData.tempStore.oauthObject.applicationId) {
                accountDetails.applicationId = requestData.tempStore.oauthObject.applicationId;
            }

            if (requestData.tempStore.oauthObject.applicationuserId) {
                accountDetails.applicationuserId = requestData.tempStore.oauthObject.applicationuserId;
            }
        }
        //Account Details 

        //Component Details  
        if (requestData.tempStore && requestData.tempStore.componentType) {
            componentDetails.componentType = "customflows";
        }

        if (requestData.tempStore && requestData.tempStore.componentCode) {
            componentDetails.componentCode = requestData.tempStore.componentCode;
        }

        if (!componentDetails.componentCode || componentDetails.componentCode == undefined) {
            componentDetails.componentCode = '';
        }
        if (componentDetails.componentType && componentDetails.componentCode) {
            let componentAdditionalCodesArray = [];
            let componentAdditionalCodesObject = {
                "codeName": componentDetails.componentType,
                "codeValue": componentDetails.componentCode
            }
            componentAdditionalCodesArray.push(componentAdditionalCodesObject);
            componentDetails.componentAdditionalCodes = componentAdditionalCodesArray;
        }
        if (!componentDetails.componentType || componentDetails.componentType == undefined) {
            componentDetails.componentType = 'general';
        }
        //Component Details

        //Request Details
        if (requestData.method) {
            requestDetails.requestType = requestData.method;
        }

        if (requestData.path) {
            requestDetails.endpoint = requestData.baseUrl + requestData.path;
        }

        let queryParamsObject = {
            "query": "",
            "params": ""
        };

        if (requestData.query) {
            queryParamsObject.query = JSON.stringify(requestData.query);
        }

        if (requestData.params) {
            queryParamsObject.params = JSON.stringify(requestData.params);
        }

        requestDetails.queryParams = queryParamsObject;

        if (requestData.headers && requestData.headers.host) {
            requestDetails.requestedDomain = requestData.headers.host;
        }

        if (responseData && Array.isArray(responseData.respone)) {
            let responseObj = {};

            responseData.respone.forEach(function(resObj) {
                if (resObj && resObj.requestedAt) {
                    responseObj.requestedAt = resObj.requestedAt;
                }

                if (resObj && resObj.processedAt) {
                    responseObj.processedAt = resObj.processedAt;
                } else {
                    responseObj.processedAt = new Date().toISOString();
                }

                if (resObj && resObj.requestStatus != undefined) {
                    responseObj.requestStatus = resObj.requestStatus;
                }
            });

            requestDetails.requestedAt = responseObj.requestedAt;
            requestDetails.processedAt = responseObj.processedAt;
            requestDetails.requestStatus = responseObj.requestStatus;
        } else {
            requestDetails.requestedAt = responseData.respone.requestedAt;
            requestDetails.processedAt = responseData.respone.processedAt ? responseData.respone.processedAt : new Date().toISOString();
            requestDetails.requestStatus = responseData.respone.requestStatus;
        }

        requestDetails.responseHttpCode = statusCode;

        if (responseData && responseData.respone.errors && responseData.respone.errors.length > 0) {
            let errorCodesArray = [];
            responseData.respone.errors.forEach(function(errorObj) {
                let errorCodesObj = {};
                errorCodesObj.errorCode = errorObj.errorCode;
                errorCodesArray.push(errorCodesObj);
            });

            requestDetails.errors = errorCodesArray;
        }
        if (responseData && responseData.respone.warnings && responseData.respone.warnings.length > 0) {
            let warningCodesArray = [];
            responseData.respone.warnings.forEach(function(warningObj) {
                let warningCodesObj = {};
                warningCodesObj.warningCode = warningObj.warningCode;
                warningCodesArray.push(warningCodesObj);
            });

            requestDetails.warnings = warningCodesArray;
        }

        if (requestData.headers && requestData.headers['content-length']) {
            requestDetails.requestSize = parseFloat(requestData.headers['content-length']);
        }

        if (responseSize) {
            requestDetails.responseSize = responseSize;
        }

        trackingInfoObj.accountDetails = accountDetails;
        trackingInfoObj.clientDetails = requestData.tempStore.clientDetails;
        trackingInfoObj.requestDetails = requestDetails;
        trackingInfoObj.componentDetails = componentDetails;
        if (requestData.tempStore.componentDetails) {
            trackingInfoObj.componentDetails = requestData.tempStore.componentDetails;
        }

        return trackingInfoObj;

    }

    function getSizeOfResponse(response) {
        let responseList = [];
        let stack = [response];
        let bytes = 0;
        while (stack.length) {
            let value = stack.pop();
            if (typeof value === 'boolean') {
                bytes += 4;
            } else if (typeof value === 'string') {
                bytes += value.length * 2;
            } else if (typeof value === 'number') {
                bytes += 8;
            } else if (typeof value === 'object' && responseList.indexOf(value) === -1) {
                responseList.push(value);
                for (let i in value) {
                    stack.push(value[i]);
                }
            }
        }
        return bytes;
    }

    function rawBodyParser(req, res, next) {
        if (req.skipRawBodyParser) { next(); } // don't parse this if told to skip
        if (req._body) { return next(); }
        req.body = "";
        req._body = true;
        var isText = true;
        var checkUTF = false;
        if (req.headers['content-type']) {
            var parsedType = typer.parse(req.headers['content-type'])
            if (parsedType.type === "text") {
                isText = true;
            } else if (parsedType.subtype === "xml" || parsedType.suffix === "xml") {
                isText = true;
            } else if (parsedType.type !== "application") {
                isText = false;
            } else if (parsedType.subtype !== "octet-stream") {
                checkUTF = true;
            } else {
                // applicatino/octet-stream
                isText = false;
            }
        }
        getBody(req, {
            length: req.headers['content-length'],
            encoding: isText ? "utf8" : null
        }, function(err, buf) {
            if (err) { return next(err); }
            if (!isText && checkUTF && isUtf8(buf)) {
                buf = buf.toString()
            }
            req.body = buf;
            next();
        });
    }
    var corsSetup = false;

    function createRequestWrapper(node, req) {
        // This misses a bunch of properties (eg headers). Before we use this function
        // need to ensure it captures everything documented by Express and HTTP modules.
        var wrapper = {
            _req: req
        };
        var toWrap = [
            "param",
            "get",
            "is",
            "acceptsCharset",
            "acceptsLanguage",
            "app",
            "baseUrl",
            "body",
            "cookies",
            "fresh",
            "hostname",
            "ip",
            "ips",
            "originalUrl",
            "params",
            "path",
            "protocol",
            "query",
            "route",
            "secure",
            "signedCookies",
            "stale",
            "subdomains",
            "xhr",
            "socket" // TODO: tidy this up
        ];
        toWrap.forEach(function(f) {
            if (typeof req[f] === "function") {
                wrapper[f] = function() {
                    node.warn(RED._("httpin.errors.deprecated-call", { method: "msg.req." + f }));
                    var result = req[f].apply(req, arguments);
                    if (result === req) {
                        return wrapper;
                    } else {
                        return result;
                    }
                }
            } else {
                wrapper[f] = req[f];
            }
        });
        return wrapper;
    }

    function createResponseWrapper(node, res) {
        var wrapper = {
            _res: res
        };
        var toWrap = [
            "append",
            "attachment",
            "cookie",
            "clearCookie",
            "download",
            "end",
            "format",
            "get",
            "json",
            "jsonp",
            "links",
            "location",
            "redirect",
            "render",
            "send",
            "sendfile",
            "sendFile",
            "sendStatus",
            "set",
            "status",
            "type",
            "vary"
        ];
        toWrap.forEach(function(f) {
            wrapper[f] = function() {
                node.warn(RED._("httpin.errors.deprecated-call", { method: "msg.res." + f }));
                var result = res[f].apply(res, arguments);
                if (result === res) {
                    return wrapper;
                } else {
                    return result;
                }
            }
        });
        return wrapper;
    }
    var corsHandler = function(req, res, next) { next(); }
    if (RED.settings.httpNodeCors) {
        corsHandler = cors(RED.settings.httpNodeCors);
        RED.httpNode.options("*", corsHandler);
    }

    function HTTPIn(n) {
        RED.nodes.createNode(this, n);
        if (RED.settings.httpNodeRoot !== false) {
            if (!n.url) {
                this.warn(RED._("httpin.errors.missing-path"));
                return;
            }
            this.url = `${n.controllerId}/` + n.url;
            this.url = this.url.replace(/\/\//g, '/');
            if (this.url[0] !== '/') {
                this.url = '/' + this.url;
            }
            this.method = n.method;
            this.upload = n.upload;
            this.swaggerDoc = n.swaggerDoc;
            var node = this;
            this.errorHandler = function(err, req, res, next) {
                node.warn(err);
                res.sendStatus(500);
            };
            this.callback = function(req, res) {
                var msgid = RED.util.generateId();
                res._msgid = msgid;
                if (node.method.match(/^(post|delete|put|options|patch)$/)) {
                    node.send({ _msgid: msgid, req: req, res: createResponseWrapper(node, res), payload: req.body });
                } else if (node.method == "get") {
                    node.send({ _msgid: msgid, req: req, res: createResponseWrapper(node, res), payload: req.query });
                } else {
                    node.send({ _msgid: msgid, req: req, res: createResponseWrapper(node, res) });
                }
            };
            var httpMiddleware = function(req, res, next) { next(); }
            if (RED.settings.httpNodeMiddleware) {
                if (typeof RED.settings.httpNodeMiddleware === "function") {
                    httpMiddleware = RED.settings.httpNodeMiddleware;
                }
            }
            var maxApiRequestSize = RED.settings.apiMaxLength || '5mb';
            var jsonParser = bodyParser.json({ limit: maxApiRequestSize });
            var urlencParser = bodyParser.urlencoded({ limit: maxApiRequestSize, extended: true });
            var metricsHandler = function(req, res, next) { next(); }
            if (this.metric()) {
                metricsHandler = function(req, res, next) {
                    var startAt = process.hrtime();
                    onHeaders(res, function() {
                        if (res._msgid) {
                            var diff = process.hrtime(startAt);
                            var ms = diff[0] * 1e3 + diff[1] * 1e-6;
                            var metricResponseTime = ms.toFixed(3);
                            var metricContentLength = res._headers["content-length"];
                            //assuming that _id has been set for res._metrics in HttpOut node!
                            node.metric("response.time.millis", { _msgid: res._msgid }, metricResponseTime);
                            node.metric("response.content-length.bytes", { _msgid: res._msgid }, metricContentLength);
                        }
                    });
                    next();
                };
            }
            var multipartParser = function(req, res, next) { next(); }
            if (this.upload) {
                var mp = multer({ storage: multer.memoryStorage() }).any();
                multipartParser = function(req, res, next) {
                    mp(req, res, function(err) {
                        req._body = true;
                        next(err);
                    })
                };
            }
            if (this.method == "get") {
                RED.httpNode.get(this.url, cookieParser(), httpMiddleware, corsHandler, metricsHandler, this.callback, this.errorHandler);
            } else if (this.method == "post") {
                RED.httpNode.post(this.url, cookieParser(), httpMiddleware, corsHandler, metricsHandler, jsonParser, urlencParser, multipartParser, rawBodyParser, this.callback, this.errorHandler);
            } else if (this.method == "put") {
                RED.httpNode.put(this.url, cookieParser(), httpMiddleware, corsHandler, metricsHandler, jsonParser, urlencParser, rawBodyParser, this.callback, this.errorHandler);
            } else if (this.method == "patch") {
                RED.httpNode.patch(this.url, cookieParser(), httpMiddleware, corsHandler, metricsHandler, jsonParser, urlencParser, rawBodyParser, this.callback, this.errorHandler);
            } else if (this.method == "delete") {
                RED.httpNode.delete(this.url, cookieParser(), httpMiddleware, corsHandler, metricsHandler, jsonParser, urlencParser, rawBodyParser, this.callback, this.errorHandler);
            }
            this.on("close", function() {
                var node = this;
                RED.httpNode._router.stack.forEach(function(route, i, routes) {
                    if (route.route && route.route.path === node.url && route.route.methods[node.method]) {
                        routes.splice(i, 1);
                    }
                });
            });
        } else {
            this.warn(RED._("httpin.errors.not-created"));
        }
    }
    RED.nodes.registerType("http in", HTTPIn);

    function HTTPOut(n) {
        RED.nodes.createNode(this, n);
        var node = this;
        this.headers = n.headers || {};
        this.statusCode = n.statusCode;
        this.on("input", function(msg) {
            if (msg.res) {
                var headers = RED.util.cloneMessage(node.headers);
                if (msg.headers) {
                    if (msg.headers.hasOwnProperty('x-node-red-request-node')) {
                        var headerHash = msg.headers['x-node-red-request-node'];
                        delete msg.headers['x-node-red-request-node'];
                        var hash = hashSum(msg.headers);
                        if (hash === headerHash) {
                            delete msg.headers;
                        }
                    }
                    if (msg.headers) {
                        for (var h in msg.headers) {
                            if (msg.headers.hasOwnProperty(h) && !headers.hasOwnProperty(h)) {
                                headers[h] = msg.headers[h];
                            }
                        }
                    }
                }
                if (Object.keys(headers).length > 0) {
                    msg.res._res.set(headers);
                }
                if (msg.cookies) {
                    for (var name in msg.cookies) {
                        if (msg.cookies.hasOwnProperty(name)) {
                            if (msg.cookies[name] === null || msg.cookies[name].value === null) {
                                if (msg.cookies[name] !== null) {
                                    msg.res._res.clearCookie(name, msg.cookies[name]);
                                } else {
                                    msg.res._res.clearCookie(name);
                                }
                            } else if (typeof msg.cookies[name] === 'object') {
                                msg.res._res.cookie(name, msg.cookies[name].value, msg.cookies[name]);
                            } else {
                                msg.res._res.cookie(name, msg.cookies[name]);
                            }
                        }
                    }
                }
                var statusCode = node.statusCode || msg.statusCode || 200;
                saveTrackingData(msg, statusCode);
                if (typeof msg.payload == "object" && !Buffer.isBuffer(msg.payload)) {
                    msg.res._res.status(statusCode).jsonp(msg.payload);
                } else {
                    if (msg.res._res.get('content-length') == null) {
                        var len;
                        if (msg.payload == null) {
                            len = 0;
                        } else if (Buffer.isBuffer(msg.payload)) {
                            len = msg.payload.length;
                        } else if (typeof msg.payload == "number") {
                            len = Buffer.byteLength("" + msg.payload);
                        } else {
                            len = Buffer.byteLength(msg.payload);
                        }
                        msg.res._res.set('content-length', len);
                    }
                    if (typeof msg.payload === "number") {
                        msg.payload = "" + msg.payload;
                    }
                    msg.res._res.status(statusCode).send(msg.payload);
                }
            } else {
                node.warn(RED._("httpin.errors.no-response"));
            }
        });
    }
    RED.nodes.registerType("http response", HTTPOut);
}