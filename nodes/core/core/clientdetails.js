module.exports = function(RED) {
    var geoip = require('geoip-lite');

    function clientdetails(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', function(msg) {
            let ipAddr = '';
            let clientDetails = {};
            let lat = 0;
            let lon = 0;
            let geo = {};
            let city = 'Unknown';
            let state = 'Unknown';
            let country = 'Unknown';
            if (msg.req.header && msg.req.header('_ip')) {                
                ipAddr = msg.req.header('_ip')
            } else if (msg.req.header && msg.req.header('X-Forwarded-For')) {
                ipAddr = msg.req.header('X-Forwarded-For');
                ipAddr = ipAddr.split(",")[0];
            } else if (msg.req.headers && msg.req.headers['x-forwarded-for']) {
                ipAddr = msg.req.headers['x-forwarded-for'];
                ipAddr = ipAddr.split(",")[0];
            } else if (msg.req.connection && msg.req.connection.remoteAddress) {
                ipAddr = msg.req.connection.remoteAddress;
            } else {
                ipAddr = msg.req.ip;
            }

            if (ipAddr) {
                if (ipAddr === "127.0.0.1" || ipAddr === "::ffff:127.0.0.1" || ipAddr === "::1") {
                    ipAddr = "0.0.0.0";
                } else {
                    if (ipAddr) {
                        geo = geoip.lookup(ipAddr);
                        if (geo && geo.ll) {
                            if (geo.ll[0]) {
                                lat = geo.ll[0];
                            }

                            if (geo.ll[1]) {
                                lon = geo.ll[1];
                            }
                        }
                        if (geo) {
                            if (geo.city) {
                                city = geo.city;
                            }

                            if (geo.region) {
                                state = geo.region;
                            }

                            if (geo.country) {
                                country = geo.country;
                            }
                        }

                    }
                }
            } else {
                ipAddr = "0.0.0.0";
            }

            clientDetails = {
                "location": {
                    "lat": lat,
                    "lon": lon,
                },
                "ipAddress": ipAddr,
                "city": city,
                "state": state,
                "country": country
            };

            //response payload            
            msg.payload.clientDetails = clientDetails;
            node.send(msg);
        });
    }
    RED.nodes.registerType("client details", clientdetails);
}